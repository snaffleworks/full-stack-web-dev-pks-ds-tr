<?php

namespace App\Http\Controllers;

use App\post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    public function index()
    

    {
        $post = post::latest()->get();
        
        return response()->json([
            'succes' => true,
            'message'=> 'Data Accpeted',
            'data'  => $post
        ]);
    }

    public function store(Request $request)
    {
        $allrequest = $request->all();

        $validator = Validator::make($allrequest ,[
            'title' => 'required',
            'description' =>'required',

        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $post = post::create([
            'title' => $request->title,
            'descrption' => $request->description,
        ]);

        if($post){

        return response()->json([
            'succes' => true,
            'message'=> 'Data Accpeted',
            'data'  => $post
        ],200);
        }

        return response()->json([
            'succes' => false,
            'message'=> 'Data Not Accpeted',
        ], 409);

    }

    public function show($id)
    {
        $post = Post::findOrfail($id);

        return response()->json([
            'succes' => true,
            'message'=> 'Data showed',
            'data'=> '$post'
        ], 200);
    }

    



}
