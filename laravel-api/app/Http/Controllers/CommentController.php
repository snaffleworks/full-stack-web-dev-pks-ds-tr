<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Validation\Validator;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $post_id = $request->post_id;

        $comments = Comment::where('post_id' ,$post_id)->latest()->get();
        
        return response()->json([
            'succes' => true,
            'message'=> 'Data Accpeted',
            'data'  => $comments
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allrequest = $request->all();

        $validator = Validator::make($allrequest ,[
            'content' => 'required',
            'post_id' =>'required'

        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $comments = Comment::create([
            'content' => $request->title,
            'post_id' => $request->post-id
        ]);

        if($comments){

        return response()->json([
            'succes' => true,
            'message'=> 'Data Accpeted',
            'data'  => $comments
        ],200);
        }

        return response()->json([
            'succes' => false,
            'message'=> 'Data Not Accpeted'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comments = Comment::find($id);

        if($comments){

            return response()->json([
                'succes' => true,
                'message'=> 'Data Accpeted',
                'data'  => $comments
            ],200);

        return response()->json([
            'succes' => false,
            'message'=> 'Data : '.$id.' not found',
            ], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)

    {
        $allrequest = $request->all();

        $validator = Validator::make($allrequest,[
            'content' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $comments = Comment::find($id);

        if ($comments) {
            $comments->update([
                'content' => $comments->content,
            ]);

            return response()->json([
                'succes' => true,
                'message'=> 'Data updated',
                'data'=> $comments
            ]);
        }
            return response()->json([
            'succes' => false,
            'message'=> 'Data : '.$id.' not found',
            ], 404);

        
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comments = Comment::find($id);

        if ($comments) {
            $comments->delete();

            return response()->json([
                'succes' => true,
                'message'=> 'Data deleted',
                
            ] ,200);
            
        }

        return response()->json([
            'succes' => false,
            'message'=> 'Data : '.$id.' not found',
            ], 404);
    }
}
