<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RegenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allrequest = $request->all();

        $validator = Validator::make($allrequest ,[
            'email' => 'required',
            
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }
        $user = User::where('email', $request->email)->first();

        dd(user->otp_code());

        $user->otp_code()->delete();

        do {

            $random = mt_rand(100000 , 999999);
            $check = OtpCode::where('otp',$random)->first();

        }while ($check);

    }
}
