<?php


use Illuminate\Http\Request;
//use Illuminate\Routing\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::get('/post', 'PostController@index');
//Route::post('/post', 'PostController@store');
//Route::get('/post/{id}', 'PostController@show');

Route::apiResource('post', 'PostController');
Route::apiResource('comments', 'CommentController');
Route::apiResource('roles', 'RolesController');

Route::group([
    'prefix' => 'auth',
    'namespace' => 'Auth'
], function(){
Route::post('register', 'RegisterController')->name('auth.register');
Route::post('regenerate-Otp-code', 'RegenerateOtpCodeController')->name('auth.regenerate-Otp-code');
});