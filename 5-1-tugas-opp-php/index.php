<?php

trait Hewan {
    public $nama;
    public $darah = 50;
    public $jumlahkaki;
    public $keahlian;
}

abstract class Fight {
    use Hewan;
    public $attackpower;
    public $defensepower;

    public function serang($hewan)
    {
        echo "{$this->nama} sedang menyerang {$hewan->nama}";
        echo "<br>";
        $hewan->diserang($this);
    }

    public function diserang($hewan)
    {
        echo "{$this->nama} sedang diserang {$hewan->nama}";

        $this->darah = $this->darah - ($hewan->attackpower / $this->defensepower);
    }

    abstract public function getinfohewan();


}

class Elang extends Fight {

    public function __construct($nama)
    {
        $this->jumlahkaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackpower = 10;
        $this->defensepower = 5;
    }

    public function getinfohewan()
    {
        echo "jenis hewan : Elang";
        echo "<br>";
        echo "nama : {$this->nama}";
        echo "<br>";
        echo "jumlah kaki : {$this->jumlahkaki}";
        echo "<br>";
        echo "keahlian : {$this->keahlian}";
        echo "<br>";
        echo "attack power : {$this->attackpower}";
        echo "<br>";
        echo "defense power : {$this->defensepower}";
    }

}

class Harimau extends Fight {

    public function __construct($nama)
    {
        $this->jumlahkaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackpower = 7;
        $this->defensepower = 8;
    }
    public function getinfohewan()
    {
        echo "jenis hewan : Harimau";
        echo "<br>";
        echo "nama : {$this->nama}";
        echo "<br>";
        echo "jumlah kaki : {$this->jumlahkaki}";
        echo "<br>";
        echo "keahlian : {$this->keahlian}";
        echo "<br>";
        echo "attack power : {$this->attackpower}";
        echo "<br>";
        echo "defense power : {$this->defensepower}";
    }

}

$harimau = new Harimau("maumau");
$harimau->getinfohewan();
echo "<br>";
echo "-_-_-_-_-_-_-_-_-_-_-";
echo "<br>";
$elang = new Elang("langlang");
$elang->getinfohewan();
echo "<br>";
echo "\===========/";
echo "<br>";

$harimau->serang($elang);
?>